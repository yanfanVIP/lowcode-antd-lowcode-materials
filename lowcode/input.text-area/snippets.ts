export default [
  {
    title: '长文本',
    screenshot: '/resources/input-text-area-1.png',
    schema: {
      componentName: 'Input.TextArea',
      props: {
        autoSize: {
          minRows: 3,
          maxRows: 3,
        },
        placeholder: '请输入',
      },
    },
  },
];
