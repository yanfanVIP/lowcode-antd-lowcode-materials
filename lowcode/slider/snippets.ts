export default [
  {
    title: '滑动输入条',
    screenshot: '/resources/slider-1.png',
    schema: {
      componentName: 'Slider',
      props: {
        defaultValue: 30,
      },
    },
  },
];
