export default [
  {
    title: '图标',
    screenshot: '/resources/icon-1.jpg',
    schema: {
      componentName: 'Icon',
      props: {
        type: 'SmileOutlined',
        size: 20,
      },
    },
  },
];
