export default [
  {
    title: '徽标数',
    screenshot: '/resources/badge-1.png',
    schema: {
      componentName: 'Badge',
      props: {
        count: 25,
      },
    },
  },
];
