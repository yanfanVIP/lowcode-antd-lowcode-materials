export default [
  {
    title: '图片',
    screenshot: '/resources/image-1.png',
    schema: {
      componentName: 'Image',
      props: {
        src: 'https://zos.alipayobjects.com/rmsportal/jkjgkEfvpUPVyRjUImniVslZfWPnJuuZ.png',
        height: 120,
        width: 120,
      },
    },
  },
];
