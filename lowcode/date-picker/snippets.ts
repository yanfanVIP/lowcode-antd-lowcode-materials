export default [
  {
    title: '选择日期',
    screenshot: '/resources/date-picker-1.png',
    schema: {
      componentName: 'DatePicker',
      props: {},
    },
  },
  {
    title: '选择周',
    screenshot: '/resources/date-picker-2.png',
    schema: {
      componentName: 'DatePicker',
      props: {
        picker: 'week',
      },
    },
  },
  {
    title: '选择月份',
    screenshot: '/resources/date-picker-3.png',
    schema: {
      componentName: 'DatePicker',
      props: {
        picker: 'month',
      },
    },
  },
  {
    title: '选择季度',
    screenshot: '/resources/date-picker-4.png',
    schema: {
      componentName: 'DatePicker',
      props: {
        picker: 'quarter',
      },
    },
  },
  {
    title: '选择年份',
    screenshot: '/resources/date-picker-5.png',
    schema: {
      componentName: 'DatePicker',
      props: {
        picker: 'year',
      },
    },
  },
];
