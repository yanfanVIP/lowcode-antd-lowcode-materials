export default [
  {
    title: '空状态',
    screenshot: '/resources/empty-1.png',
    schema: {
      componentName: 'Empty',
      props: {},
    },
  },
];
