export default [
  {
    title: '表单项',
    screenshot: '/resources/form-item-1.jpg',
    schema: {
      componentName: 'Form.Item',
      props: {
        label: '表单项',
      },
    },
  },
];
