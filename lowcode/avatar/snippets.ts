export default [
  {
    title: '头像',
    screenshot: '/resources/avatar-1.jpg',
    schema: {
      componentName: 'Avatar',
      props: {
        src: '/resources/avatar.png',
      },
    },
  },
];
