export default [
  {
    title: '骨架屏',
    screenshot: '/resources/skeleton-1.png',
    schema: {
      componentName: 'Skeleton',
      props: {
        active: true,
        loading: true 
      },
    },
  },
];
