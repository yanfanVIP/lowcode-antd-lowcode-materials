export default [
  {
    title: '描述列表',
    screenshot: '/resources/descriptions-1.jpg',
    schema: {
      componentName: 'Descriptions',
      props: {
        title: '用户信息',
        items: [
          {
            label: '用户名',
            children: 'Mo Yao',
          },
        ],
      },
    },
  },
];
