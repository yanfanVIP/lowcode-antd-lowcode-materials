export default [
  {
    title: '数字输入框',
    screenshot: '/resources/input-number-1.png',
    schema: {
      componentName: 'InputNumber',
      props: {
        placeholder: '请输入',
      },
    },
  },
];
