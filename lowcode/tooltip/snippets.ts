export default [
  {
    title: '文字提示',
    screenshot: '/resources/tooltip-1.jpg',
    schema: {
      componentName: 'Tooltip',
      props: {
        title: '提示内容',
      },
    },
  },
];
