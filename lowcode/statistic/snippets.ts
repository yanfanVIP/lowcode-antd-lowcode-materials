export default [
  {
    title: '统计数值',
    screenshot: '/resources/statistic-1.png',
    schema: {
      componentName: 'Statistic',
      props: {
        title: 'Active Users',
        value: 16589,
      },
    },
  },
];
