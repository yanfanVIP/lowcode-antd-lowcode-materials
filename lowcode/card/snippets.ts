export default [
  {
    title: '卡片',
    screenshot: '/resources/card-1.png',
    schema: {
      componentName: 'Card',
      props: {
        title: 'Default size card',
      },
    },
  },
];
