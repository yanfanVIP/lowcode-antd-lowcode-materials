export default [
  {
    title: '时间轴',
    screenshot: '/resources/timeline-1.jpg',
    schema: {
      componentName: 'Timeline',
      props: {
        steps: [
          {
            key: 'timeLinei5wd',
            label: '时间轴',
          },
          {
            key: 'timeLinei5wx',
            label: '时间轴',
          },
        ],
      },
    },
  },
];
