export default [
  {
    title: '页头',
    screenshot: '/resources/page-header-1.jpg',
    schema: {
      componentName: 'PageHeader',
      props: {
        title: 'Title',
        subTitle: 'This is a subtitle',
      },
    },
  },
];
