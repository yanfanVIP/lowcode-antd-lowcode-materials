export default [
  {
    title: '搜索框',
    screenshot: '/resources/input-search-1.png',
    schema: {
      componentName: 'Input.Search',
      props: {
        placeholder: '搜索...',
      },
    },
  },
];
