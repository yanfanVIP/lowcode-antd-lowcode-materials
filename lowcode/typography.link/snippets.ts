export default [
  {
    title: '链接',
    screenshot: '/resources/typography-link-1.png',
    schema: {
      componentName: 'Typography.Link',
      props: {
        href: 'https://alibaba.com',
        target: '_blank',
        children: '链接',
      },
    },
  },
];
