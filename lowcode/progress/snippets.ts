export default [
  {
    title: '进度条',
    screenshot: '/resources/progress-1.png',
    schema: {
      componentName: 'Progress',
      props: {
        percent: 20,
        status: 'active',
      },
    },
  },
  {
    title: '进度圈',
    screenshot: '/resources/progress-2.png',
    schema: {
      componentName: 'Progress',
      props: {
        percent: 20,
        type: 'circle',
      },
    },
  },
];
