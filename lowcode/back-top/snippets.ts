export default [
  {
    title: '回到顶部',
    screenshot: '/resources/back-top-1.jpg',
    schema: {
      componentName: 'BackTop',
      props: {},
    },
  },
];
