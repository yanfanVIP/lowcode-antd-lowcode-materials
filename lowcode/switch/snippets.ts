export default [
  {
    title: '开关',
    screenshot: '/resources/switch-1.png',
    schema: {
      componentName: 'Switch',
      props: {
        defaultChecked: true,
      },
    },
  },
];
