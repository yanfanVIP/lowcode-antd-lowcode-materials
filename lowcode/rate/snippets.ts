export default [
  {
    title: '评分',
    screenshot: '/resources/rate-1.png',
    schema: {
      componentName: 'Rate',
      props: {
        defaultValue: 3,
      },
    },
  },
];
