export default [
  {
    title: '多选框',
    screenshot: '/resources/checkbox-1.png',
    schema: {
      componentName: 'Checkbox',
      props: {
        children: 'Checkbox',
      },
    },
  },
  {
    title: '多选框组',
    screenshot: '/resources/checkbox-group-1.png',
    schema: {
      componentName: 'Checkbox.Group',
      props: {
        options: [
          {
            label: 'A',
            value: 'A',
          },
          {
            label: 'B',
            value: 'B',
          },
          {
            label: 'C',
            value: 'C',
          },
        ],
      },
    },
  },
];
