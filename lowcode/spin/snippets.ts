export default [
  {
    title: '加载中',
    screenshot: '/resources/spin-1.png',
    schema: {
      componentName: 'Spin',
      props: {
        size: 'large',
        tip: 'loading...',
      },
    },
  },
];
