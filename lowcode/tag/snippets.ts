export default [
  {
    title: '标签',
    screenshot: '/resources/tag-1.png',
    schema: {
      componentName: 'Tag',
      props: {
        color: 'magenta',
        children: 'tag',
      },
    },
  },
];
