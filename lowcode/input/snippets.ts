export default [
  {
    title: '输入框',
    screenshot: '/resources/input-1.png',
    schema: {
      componentName: 'Input',
      props: {
        placeholder: '请输入',
      },
    },
  },
];
