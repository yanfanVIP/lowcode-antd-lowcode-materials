const { name, version } = require("./package.json");

const library = 'AntdLowcode';

module.exports = {
  sourceMap: false,
  alias: {
    '@': './src',
    lowcode: './lowcode'
  },
  devServer: {
    proxy: {
      "/": "http://localhost:3000"
    }
  },
  plugins: [
    [
      '@alifd/build-plugin-lowcode',
      {
        noParse: true,
        engineScope: '@alilc',
        library,
        staticResources: {
          engineCoreCssUrl: '/resources/lowcode-engine-core-1.2.5.css',
          engineExtCssUrl: '/resources/lowcode-engine-ext-1.0.6.css',
          engineCoreJsUrl: '/resources/lowcode-engine-core-1.2.5.js',
          engineExtJsUrl: '/resources/lowcode-engine-ext-1.0.6.js',
        },
        npmInfo: {
          package: name,
          version,
        },
        lowcodeDir: 'lowcode',
        entryPath: 'src/index.tsx',
        categories: ['通用', '导航', '信息输入', '信息展示', '信息反馈'],
        baseUrl: {
          prod: `/resources/${name}@${version}`,
          daily: `/resources/${name}@${version}`,
        },
        builtinAssets: [
          {
            packages: [
              {
                package: 'moment',
                version: '2.24.0',
                urls: ['/resources/g.alicdn.com_mylib_moment_2.24.0_min_moment.min.js'],
                library: 'moment',
              },
              {
                package: 'lodash',
                library: '_',
                urls: ['/resources/g.alicdn.com_platform_c_lodash_4.6.1_lodash.min.js'],
              },
              {
                package: 'iconfont-icons',
                urls: '/fonts/font_2369445_ukrtsovd92r.js',
              },
              {
                package: '@ant-design/icons',
                version: '4.7.0',
                urls: [`/resources/g.alicdn.com_code_npm_@ali_ant-design-icons-cdn_4.5.0_index.umd.min.js`],
                library: 'icons',
              },
              {
                package: 'antd',
                version: '4.23.0',
                urls: [
                  '/resources/g.alicdn.com_code_lib_antd_4.23.0_antd.min.css',
                  '/resources/g.alicdn.com_code_lib_antd_4.23.0_antd.min.js'
                ],
                library: 'antd',
              },
            ],
            components: [],
          },
        ],
      },
    ],
  ],
};
